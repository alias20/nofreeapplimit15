#import <substrate.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MIFreeProfileValidatedAppTracker : NSObject
{
    NSMutableSet *_refs;
}
-(NSMutableSet *)refs;
@end

%hook MIFreeProfileValidatedAppTracker
- (BOOL)_onQueue_addReferenceForApplicationIdentifier:(id)arg1 bundle:(id)arg2 error:(id *)arg3 {
	MSHookIvar<NSMutableSet *>(self, "_refs") = [[NSMutableSet alloc] init];
	return %orig;
}
%end

%hook NSUserDefaults
- (id)objectForKey:(NSString *)defaultName {
	id ret = %orig;
	if([defaultName isEqualToString:@"activeAppsLimit"])
		return nil;
	return ret;
}
%end
